package com.example.demo.bo;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

public class BaseBo implements Serializable {

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
