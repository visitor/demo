package com.example.demo.controller;

import com.example.demo.service.CityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Api(value="测试接口",tags = { "测试接口" })
@Controller
@RequestMapping("fastjson")
public class FastJsonController {

    @Autowired
    private CityService cityService;

    @ApiOperation("获取用户信息")
    @ApiImplicitParam(name = "name", value = "用户名", dataType = "string", paramType = "query")
    @RequestMapping("/test.do")
    @ResponseBody
    public Object test(Integer id) {
//        User user = new User();
//        user.setId(1);
//        user.setUsername("jack");
//        user.setPassword("jack123");
//        user.setBirthday(new Date());
        //int i = 1/0;
       // return user;
         return cityService.getCity(id);
    }
}
