package com.example.demo.controller;

import com.example.demo.config.ClassConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SampleController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${hello}")
    private String hello;

    @Value("${class.schoolName}")
    private String schoolName;

    @Autowired
    private ClassConfig classConfig;

    @RequestMapping("/index.do")
    public String home() {
        logger.info(hello);
        logger.info("{}", schoolName);
        logger.info("{}", classConfig);
        return "hello";
    }

    @RequestMapping("/test.do")
    public String test() {
        return "test";
    }
}
