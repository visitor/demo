package com.example.demo.controller;

import com.example.demo.model.Country;
import com.example.demo.service.ICountryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/country")
@Controller
public class CountryController {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    ICountryService countryService;

    @RequestMapping("/getList")
    @ResponseBody
    public List<Country> getList() {
        logger.info("getList");
        return countryService.getList();
    }

    @RequestMapping("/getById")
    @ResponseBody
    public Country getById(Integer id) {
        return countryService.getCountryById(id);
    }

    @RequestMapping("/setCountry")
    @ResponseBody
    public int setCountry(Country country) {
        logger.info("setCountry: country={}", country);
       return countryService.setCountry(country);
    }
}
