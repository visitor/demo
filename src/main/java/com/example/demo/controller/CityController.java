package com.example.demo.controller;

import com.example.demo.bo.CityReqBo;
import com.example.demo.service.ICityService;
import com.example.demo.vo.CityRespVo;
import com.example.demo.vo.ResultRespVo;
import io.swagger.models.auth.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import javax.annotation.Resource;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/city")
@Controller
public class CityController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    ICityService cityService;

    @RequestMapping("/getCity")
    @ResponseBody
    public CityRespVo getCity(@RequestParam(value = "id", defaultValue = "0") Integer id) {
        logger.info("Enter getCity: id ={}", id);
        return cityService.getCity(id);
    }

    @RequestMapping("/getCityByIdAndName/{id}/{name}")
    @ResponseBody
    public CityRespVo getCityByIdAndName(@PathVariable("id") Integer id,
                                         @PathVariable(value = "name") String name) {
        logger.info("Enter getCityByIdAndName: id={}, name={}", id, name);
        return cityService.getCityByIdAndName(id, name);
    }

    @RequestMapping("/addCity")
    @ResponseBody
    public ResultRespVo addCity(CityReqBo cityReqBo) {
        logger.info("Enter addCity: cityReqBo={}", cityReqBo);
        ResultRespVo resultRespVo = new ResultRespVo();
        cityService.setCity(cityReqBo);
        resultRespVo.setCode(200);
        resultRespVo.setMessage("success");
        return resultRespVo;
    }

    @RequestMapping("/getCityList")
    @ResponseBody
    public List<CityRespVo> getCityList(String ids) {
        logger.info("Enter getCityList: ids={}", ids);
        List<Integer> idList = new ArrayList<Integer>();
        if(!StringUtils.isEmpty(ids)) {
             for(String str : ids.split(",")) {
                 if(str != null){
                     idList.add(Integer.valueOf(str));
                 }
             }
        }
        return cityService.getCityList(idList);
    }



    @RequestMapping("/deleteCity")
    @ResponseBody
    public ResultRespVo deleteCity(Integer id) {
        logger.info("Enter deleteCity : id={}", id);
        ResultRespVo resultRespVo = new ResultRespVo();
        cityService.deleteCity(id);
        resultRespVo.setCode(200);
        resultRespVo.setMessage("success");
        return resultRespVo;
    }
}
