package com.example.demo.controller;

import com.example.demo.model.City;
import io.swagger.models.auth.In;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.commons.lang3.*;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.CollectionUtils;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;

public class Main {

    public static void main(String[] args) throws Exception {
        HashMap<String, String> map = new HashMap<>();

        map.put("a","123");
        map.put("b","456");
        for (String val : map.values()) {
            System.out.println(val);
        }
        for(String key:map.values()) {
            System.out.println(key);
        }
        String a= "123";

        int ab = Integer.parseInt(a);
        System.out.println(StringUtils.isNumeric(a));;
        System.out.println(ab);

        Integer[] nums = {1,2,3,4};
        String[] test1 = {"123","456"};
        String[] test2 = {"456", "888"};
        System.out.println(ArrayUtils.contains(nums,1));
        List<Integer> list = CollectionUtils.arrayToList(nums);
        System.out.println(list);

        City city = new City();
        city.setCityId(1);
        city.setCountryId(123);
        city.setCity("asdfa");
        System.out.println(RandomUtils.nextInt());

        System.out.println(ClassUtils.getShortClassName(City.class));

        System.out.println(NumberUtils.isDigits("123123"));
        System.out.println(NumberUtils.toFloat("123123"));

        System.out.println(RandomStringUtils.randomAlphabetic(15));
        System.out.println(RandomStringUtils.randomAlphanumeric(10));

        System.out.println(StringEscapeUtils.escapeHtml3("</html>"));

        System.out.println(NumberUtils.max(new int[]{1,3,4}));

        System.out.println(SystemUtils.getJavaHome());
        System.out.println(SystemUtils.getJavaIoTmpDir());

        System.out.println(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));

        System.out.println(DateFormatUtils.format(DateUtils.addDays( new Date(), 7), "yyyy-MM-dd HH:mm:ss"));

        Map<String, Object> map2 = new HashMap<>();
        map2.put("city","asdfas");
        map2.put("cityId", 1);
        System.out.println(BeanUtils.describe(city));
        BeanUtils.populate(city, map2);
        System.out.println(city);
        Base64 base64 = new Base64();
        System.out.println(base64.encode(city.getCity().getBytes("utf-8")));
        System.out.println(Md5Crypt.md5Crypt("aaa".getBytes("utf-8")));
    }
}
