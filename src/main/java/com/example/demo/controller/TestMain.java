package com.example.demo.controller;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.*;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TestMain {
    public static void main(String[] args) {
        HashMap<String, String> map = Maps.newHashMap();
        map.put("aaa","123123");
        map.put("bbb","4444");
        System.out.println(map);
        String num = "123aa";
        System.out.println(NumberUtils.isDigits(num));
        System.out.println(RandomStringUtils.randomAlphabetic(10));
        System.out.println(RandomStringUtils.randomAlphanumeric(20));
        System.out.println(new SimpleDateFormat().format(new Date()));
        System.out.println(StringEscapeUtils.escapeHtml4("<h1>sdfasd</h1>"));
        Pair<String, String> pair = Pair.of("aa","bb");
        System.out.println(pair.getLeft() + pair.getValue());
        System.out.println(pair.getLeft() + pair.getRight());
        Pair<String, String> mutablePair = new MutablePair<>("123","444");
        System.out.println(mutablePair.getKey() + mutablePair.getValue());
        Pair<String, String> immutablePair = new ImmutablePair<>("22","444");
        System.out.println(immutablePair);
        System.out.println(StringEscapeUtils.unescapeHtml4("</html>"));
        String[] test = {"222","ddfff","123"};
        String[] test2 = {"222","ddfff"};
        System.out.println(ArrayUtils.isEquals(test, test2));
        System.out.println(ArrayUtils.contains(test,"222"));
        System.out.println(ArrayUtils.toString(test2));
        Map map1 = ArrayUtils.toMap(new String[][]{{"123","456"},{"555","7777"}});
        System.out.println(map1);
        System.out.println(ClassUtils.getPackageName(TestMain.class));
        System.out.println(StringEscapeUtils.escapeJava("String"));
        System.out.println(StringUtils.rightPad("AAA", 20,"333"));
        System.out.println(StringUtils.capitalize("aaa"));
        System.out.println(StringUtils.deleteWhitespace("aa ass dd d"));
        System.out.println(NumberUtils.max(new int[]{4,1,2}));
        System.out.println(SystemUtils.getJavaIoTmpDir());
        System.out.println(SystemUtils.getUserDir());
        System.out.println(SystemUtils.getUserHome());
        String str1 = "1|2|3";
        System.out.println(StringUtils.join(test, "|"));
        testCharset();
        charSetUtilsDemo();
        serializationUtilsDemo();
    }

    public static void testCharset(){
        System.out.println("TestMain.testCharset");
        CharSet charSet = CharSet.getInstance("aeiou");
        String demoStr = "The quick brown fox jumps over the lazy dog.";
        int count = 0;
        for(int i = 0 ; i< demoStr.length(); i++) {
            if(charSet.contains(demoStr.charAt(i))) {
                count++;
            }
        }
        System.out.println(count);
    }

    public static void charSetUtilsDemo() {
        System.out.println("TestMain.charSetUtilsDemo");
        System.out.println("计算字符串中包含某字符数.");
        System.out.println(CharSetUtils.count(
                "The quick brown fox jumps over the lazy dog.", "aeiou"));
        System.out.println("删除字符串中某字符.");
        System.out.println(CharSetUtils.delete(
                "The quick brown fox jumps over the lazy dog.", "aeiou"));
        System.out.println("保留字符串中某字符.");
        System.out.println(CharSetUtils.keep(
                "The quick brown fox jumps over the lazy dog.", "aeiou"));
        System.out.println("合并重复的字符.");
        System.out.println(CharSetUtils.squeeze("a  bbbbbb     c dd", "b d"));
    }

    public static void serializationUtilsDemo(){
        System.out.println("TestMain.serializationUtilsDemo");
        Date date = new Date();
        byte[] bytes = SerializationUtils.serialize(date);
        System.out.println(ArrayUtils.toString(bytes));
        System.out.println(date);
    }
}
