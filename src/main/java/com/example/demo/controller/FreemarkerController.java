package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("freemarker")
public class FreemarkerController {

    @RequestMapping("hello.do")
    public String hello(Map<String, Object> map) {
        map.put("msg", "hello Freemarker");
        return "hello";
    }
}
