package com.example.demo.service;

import com.example.demo.model.Country;

import java.util.List;

public interface ICountryService {

    public List<Country> getList();

    public Country getCountryById(Integer id);

    public Integer setCountry(Country country);
}
