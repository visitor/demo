package com.example.demo.service;

import com.example.demo.dao.CityDao;
import com.example.demo.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@CacheConfig(cacheNames = "city")
@Service(value = "cityService1")
public class CityService {

    @Autowired
    private CityDao cityDao;

    @Cacheable(key = "#id")
    public City getCity(Integer id) {
        return cityDao.getById(id);
    }
}
