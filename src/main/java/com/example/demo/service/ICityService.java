package com.example.demo.service;

import com.example.demo.bo.CityReqBo;
import com.example.demo.vo.CityRespVo;

import java.util.List;

public interface ICityService {
    public CityRespVo getCity(Integer id);

    public CityRespVo getCityByIdAndName(Integer id, String name);

    public void setCity(CityReqBo reqBo);

    public List<CityRespVo> getCityList(List<Integer> id);

    public void deleteCity(Integer id);
}
