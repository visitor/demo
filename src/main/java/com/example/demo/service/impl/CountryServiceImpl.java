package com.example.demo.service.impl;

import com.example.demo.dao.CountryDao;
import com.example.demo.model.Country;
import com.example.demo.service.ICountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CountryServiceImpl implements ICountryService {

    @Autowired
    CountryDao countryDao;

    @Override
    public List<Country> getList() {
        return countryDao.getList();
    }

    @Override
    public Country getCountryById(Integer id) {
        return countryDao.getCountryById(id);
    }

    @Override
    public Integer setCountry(Country country) {
        return countryDao.insertCountry(country);
    }
}
