package com.example.demo.service.impl;

import com.example.demo.bo.CityReqBo;
import com.example.demo.dao.CityDao;
import com.example.demo.model.City;
import com.example.demo.service.ICityService;
import com.example.demo.vo.CityRespVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ICityServiceImpl implements ICityService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    CityDao cityDao;

    @Override
    public CityRespVo getCity(Integer id) {
        logger.info("Enter ICityServiceImpl.getCity id={}", id);
        if(null != id) {
            CityRespVo cityRespVo = new CityRespVo();
            City city = cityDao.getById(id);
            if(city != null) {
                logger.info("city={}", city);
                BeanUtils.copyProperties(city, cityRespVo, CityRespVo.class);
                cityRespVo.setLastUpdatedStr(city.getLastUpdate().toString());
            }
            return cityRespVo;
        }
        return null;
    }

    @Override
    public CityRespVo getCityByIdAndName(Integer id, String name) {
        logger.info("Enter ICityServiceImpl.getCityByIdAndName: id={}, name={}", id, name);
        if(id != null && id > 0 ) {
            return this.getCity(id);
        }
        if(name != null) {
            CityRespVo cityRespVo = new CityRespVo();
            City city = cityDao.getByName(name);
            if(city != null) {
                logger.info("city={}", city);
                BeanUtils.copyProperties(city, cityRespVo, CityRespVo.class);
                cityRespVo.setLastUpdatedStr(city.getLastUpdate().toString());
            }
            return cityRespVo;
        }
        return null;
    }

    @Override
    public void setCity(CityReqBo reqBo) {
        logger.info("Enter ICityServiceImpl.setCity : reqBo={}", reqBo);
        if(reqBo != null && !StringUtils.isEmpty(reqBo.getCity()) && reqBo.getCountryId() != null) {
            City city = new City();
            city.setCountryId(reqBo.getCountryId());
            city.setCity(reqBo.getCity());
            city.setCityId(reqBo.getId());
            cityDao.insertRecord(city);
        }
    }

    @Override
    public List<CityRespVo> getCityList(List<Integer> id) {
        logger.info("Enter ICityServiceImpl.getCityList : id={}", id);
        if(id != null && id.size() > 0) {
            List<City> cityList =  cityDao.getListByIds(id);
            List<CityRespVo> cityRespVoList = new ArrayList<CityRespVo>();
            for (City city: cityList) {
                CityRespVo  cityRespVo = new CityRespVo();
                BeanUtils.copyProperties(city, cityRespVo, CityRespVo.class);
                cityRespVoList.add(cityRespVo);
            }
            return cityRespVoList;
        }
        return null;
    }

    @Override
    public void deleteCity(Integer id) {
        logger.info("Enter ICityServiceImpl.deleteCity : id={}", id);
        if(id != null) {
            cityDao.deleteById(id);
        }
    }
}
