package com.example.demo.vo;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

public class BaseBean implements Serializable {
    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
