package com.example.demo.vo;

public class CityRespVo extends BaseBean {
    private Integer cityId;

    private String city;

    private Integer countryId;

    private String lastUpdatedStr;

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getLastUpdatedStr() {
        return lastUpdatedStr;
    }

    public void setLastUpdatedStr(String lastUpdatedStr) {
        this.lastUpdatedStr = lastUpdatedStr;
    }
}
