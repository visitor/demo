package com.example.demo.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TimeAspect {

    @Pointcut("execution(* com.example.demo.controller.FastJsonController.*(..))")
    public void point() {

    }

    @Around("point()")
    public Object method(ProceedingJoinPoint joinPoint) throws Throwable{
        System.out.println("TimeAspect.method");
        Object[] args = joinPoint.getArgs();
        for (Object arg : args){
            System.out.println(arg);
        }
        long start = System.currentTimeMillis();
        Object obj = joinPoint.proceed();
        System.out.println((System.currentTimeMillis() - start));
        return obj;
    }
}
