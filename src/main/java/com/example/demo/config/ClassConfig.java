package com.example.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("classConfig")
@ConfigurationProperties(prefix = "class")
public class ClassConfig {

    private String schoolName;

    private String className;

    private List<String> students;

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<String> getStudents() {
        return students;
    }

    public void setStudents(List<String> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "ClassConfig{" +
                "schoolName='" + schoolName + '\'' +
                ", className='" + className + '\'' +
                ", students=" + students +
                '}';
    }
}
