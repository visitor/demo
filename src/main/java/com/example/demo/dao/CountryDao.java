package com.example.demo.dao;

import com.example.demo.model.Country;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface CountryDao {

    public List<Country> getList();

    public Country getCountryById(Integer id);

    int insertCountry(Country country);
}
