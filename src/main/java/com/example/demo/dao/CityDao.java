package com.example.demo.dao;

import com.example.demo.model.City;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CityDao {

    public City getById(Integer id);

    public City getByName(String name);

    public void insertRecord(City city);

    public List<City> getListByIds(List<Integer> ids);

    public void deleteById(Integer id);
}
